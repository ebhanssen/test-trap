#!perl
# -*- mode: cperl ; compile-command: "cd .. ; ./Build ; prove -vb t/prereq.t" -*-
use Test::More;

eval "use Test::Prereq::Build";

plan skip_all => "Test::Prereq::Build required to test dependencies" if $@;

prereq_ok(undef, [qw/
  PerlIO
  Test::Refcount
/]);
